<?php
    spl_autoload_register(function($classname){
        include 'libs/'.strtolower($classname).'.class.php';
    });
?>