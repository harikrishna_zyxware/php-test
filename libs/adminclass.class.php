<?php
    class AdminClass extends Database{
        public function adminlogin(){
            if(!isset($_SESSION['admin'])){
                $name = $_POST['name'];
                $password = sha1($_POST['password']);
                $query = "SELECT * FROM `admin` WHERE `name`=:name AND `password`=:password";
                $values = array(
                            ":name"=>$name,
                            ":password"=>$password
                        );
                $user = $this->query_execute($query,$values);
                if(isset($user[0]['name'])){
                    $_SESSION['admin'] = $user[0]['name'];
                    $categories = $this->get_all_categories();
                    require_once 'template/adminpanal.tpl.php';
                }
                else{
                    header("Location:http://phptest.local/index.php/");
                }
            }
            else{
                $categories = $this->get_all_categories();
                require_once 'template/adminpanal.tpl.php';
            }
        }
        public function userlogin(){
            if(!isset($_SESSION['uid'])){
                $name = $_POST['name'];
                $password = sha1($_POST['password']);
                $query = "SELECT * FROM `user` WHERE `name`=:name AND `password`=:password";
                $values = array(
                            ":name"=>$name,
                            ":password"=>$password
                        );
                $user = $this->query_execute($query,$values);
                $_SESSION['uid'] = $user[0]['id'];
                if(!isset($user)){
                    header("Location:http://phptest.local/index.php/user");
                }
                $quizes = $this->get_all_quiz();
                $marks=$this->get_all_scorebyid();
                require_once 'template/show.tpl.php';
            }
            else{
                $marks=$this->get_all_scorebyid();
                $quizes = $this->get_all_quiz();
                require_once 'template/show.tpl.php';
            }
        }
        public function get_all_categories(){
            $query = "SELECT * FROM `categories`";
            $values = array();
            $post = $this->query_execute($query,$values);
            return $post;
        }
        public function show(){
            
        }
        public function addcategory(){
            $query = "INSERT INTO `categories`(`name`,`percentage`) VALUES (:name,:percentage)";
            $values = array(
                ":name"=>$_POST['category'],
                ":percentage"=>$_POST['percent']
            );
            $this->query_execute($query,$values);
            header("Location:http://phptest.local/index.php/adminlogin");
        }
        public function addquestion(){
            $query = "INSERT INTO `question`(`question`,`cat_id`,`answer`) VALUES (:name,:id,:answer)";
            $values = array(
                ":name"=>$_POST['question'],
                ":id"=>$_POST['categoryid'],
                ":answer"=>$_POST['answer']
            );
            $this->query_execute($query,$values);
            $lid = $this->last_id;
            $query = "INSERT INTO `options`(`option1`,`option2`,`option3`,`qid`) VALUES (:option1,:option2,:option3,:qid)";
            $values = array(
                ":option1"=>$_POST['option1'],
                ":option2"=>$_POST['option2'],
                ":option3"=>$_POST['option3'],
                "qid"=>$lid
            );
            $this->query_execute($query,$values);
            header("Location:http://phptest.local/index.php/adminlogin");
        }
        public function createquiz(){
            $query = "INSERT INTO `quiz`(`name`,`no_ques`,`time_period`) VALUES (:name,:no_ques,:timeperiod)";
            $values = array(
                ":name"=>$_POST['name'],
                ":no_ques"=>$_POST['num'],
                ":timeperiod"=>$_POST['time']
            );
            $this->query_execute($query,$values);
            header("Location:http://phptest.local/index.php/adminlogin");
        }
        public function insert_mark(){
            $query = "INSERT INTO `user_score`(`userid`,`mark`,`quizid`) VALUES (:uid,:mark,:qid)";
            $values = array(
                ":uid"=>$_SESSION['uid'],
                ":mark"=>$_SESSION['mark'],
                ":qid"=>$_SESSION['quiz']['id']
            );
            $this->query_execute($query,$values);
            header("Location:http://phptest.local/index.php/adminlogin");
        }
        public function get_all_quiz(){
            $query = "SELECT * FROM `quiz`";
            $values = array();
            $post = $this->query_execute($query,$values);
            return $post;
        }
        public function get_all_scorebyid(){
            $query = "SELECT * FROM `user_score` WHERE `userid`=:uid";
            $values = array(
                    ":uid"=>$_SESSION['uid']
                );
            $post = $this->query_execute($query,$values);
            return $post;
        }
        public function get_all_quiz_by_id(){
            $query = "SELECT * FROM `quiz` WHERE `id`=:id";
            $values = array(
                ":id"=>$_GET['qid']
            );
            $post = $this->query_execute($query,$values);
            return $post[0];
        }
        public function quizstart(){
            if(!isset($_SESSION['ans'])){
                $mark=0;
                $totalnum=0;
                $quiz = $this->get_all_quiz_by_id();
                $num = $quiz['no_ques'];
                $categories = $this->get_all_categories();
                foreach($categories as $category){
                    $num_ques[$category['id']] =intval(($num*$category['percentage'])/100);
                    $totalnum += $num_ques[$category['id']];
                }
                if($totalnum<$num){
                    $num_ques = $this->smallest($num_ques);
                }
                elseif($totalnum>$num){
                    $num_ques = $this->largest($num_ques);
                }
                foreach($categories as $category){
                    $query = "SELECT * FROM `question` as q JOIN options as op ON q.id=op.qid WHERE q.cat_id=:cid";
                    $values = array(
                        ":cid"=>$category['id']
                    );
                    $quest = $this->query_execute($query,$values);
                    $questions[$category['id']] = $quest;
                }
                $this->shuffle_assoc($categories);
                foreach($categories as $category){
                    $qes = $questions[$category['id']];
                    $this->shuffle_assoc($qes);
                    $questions[$category['id']]=$qes;
                }
                $i=0;
                foreach($categories as $category){
                    $nums=$num_ques[$category['id']];
                    while($nums!=0){
                        $final[] = $questions[$category['id']][$i];
                        $nums--;
                        $i++;
                    }
                    $i=0;
                }
                shuffle($final);
                $_SESSION['ans']=$final;
                $_SESSION['mark']=$mark;
                $_SESSION['num']=$num;
                $_SESSION['quiz']=$quiz;
                $count=0;
                require_once 'template/quizstart.tpl.php';
            }
            elseif(isset($_SESSION['ans']) && isset($_GET['qid'])){
                $temp = $_GET['qid'] + 1;
                if($temp < $_SESSION['num']){
                    if($_POST['checkans']==$_POST['ans']){
                        $_SESSION['mark'] += 1;
                    }    
                    $count = $_POST['count'] + 1;
                    require_once 'template/quizstart.tpl.php';
                }
                else{
                    $this->insert_mark();
                    session_destroy();
                    header("Location:http://phptest.local/index.php/userlogin");
                }

            }
        }
        public function logout(){
            session_destroy();
            header("Location:http://phptest.local/index.php/user");
        }
        public function smallest($num_ques){
            $temp=min($num_ques);
            foreach($num_ques as $key=>$num){
                if($num_ques[$key] == $temp){
                    $num_ques[$key]+=1;
                    break;
                }
            }
            return $num_ques;
        }
        public function largest($num_ques){
            $temp=max($num_ques);
            foreach($num_ques as $key=>$num){
                if($num_ques[$key] == $temp){
                    $num_ques[$key]-=1;
                    break;
                }
            }
            return $num_ques;
        }
        public function shuffle_assoc(&$array) {
            $keys = array_keys($array);
    
            shuffle($keys);
    
            foreach($keys as $key) {
                $new[$key] = $array[$key];
            }
    
            $array = $new;
    
            return true;
        }
    }
?>