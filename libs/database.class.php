<?php
    class Database{
        public $link,$last_id;
        public function create_connection(){
            $servername = 'localhost';
            $dbname = 'phptest';
            $username = 'root';
            $password = 'root';

            $this->link = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            return $this->link;
        }
        public function query_execute($query,$values){
            $link = $this->create_connection();
            $substr = substr($query,0,6);
            if(!isset($values)){
                $result = $link->query($query);
            }
            else{
                $result = $link->prepare($query);
            }
            foreach($values as $key=>$value){
                $result->bindParam($key,$values[$key]);
            }
            $flag=$result->execute();
            if($substr == "SELECT"){
                while($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $post[] = $row;
                }
            }
            $this->last_id = $link->lastInsertId();
            $link = null; 
            return $post;
        }

    }
?>