<?php
session_start();
require_once 'autoloader.php';


$uri = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);

$adminclass = new AdminClass;

if('/index.php/' == $uri){
    require 'template/adminlogin.tpl.php';
}
if('/index.php/user' == $uri){
    require 'template/login.tpl.php';
}
else if('/index.php/adminlogin' == $uri){
    $adminclass->adminlogin();
}
else if('/index.php/userlogin' == $uri){
    $adminclass->userlogin();
}
else if('/index.php/addcategory' == $uri){
    $adminclass->addcategory();
}
else if('/index.php/addQuestion' == $uri){
    $adminclass->addquestion();
}
else if('/index.php/createquiz' == $uri){
    $adminclass->createquiz();
}
else if('/index.php/quizstart' == $uri){
    $adminclass->quizstart();
}
else if('/index.php/logout' == $uri){
    $adminclass->logout();
}
?>